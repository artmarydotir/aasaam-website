export const mutations = {
  SET_DIRECTION(state, payload) {
    state.rtlMode = payload;
  },
};

export const actions = {};

export const state = () => ({
  rtlMode: true,
});
