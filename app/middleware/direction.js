export default function ({ app, route, store }) {
  if (route.path.includes('/en')) {
    app.vuetify.framework.rtl = false;
    store.commit('SET_DIRECTION', false);
  }
}
