import { I18N } from './i18n/config';

export default {
  ssr: true,
  target: 'server',

  router: {
    base: '/',
    middleware: 'direction',
  },

  loading: {
    color: '#ff9e31',
    height: '4px',
  },
  server: {
    port: process.env.NUXT_PORT,
    host: `${process.env.NUXT_HOST}`,
    // port: 3000, // default: 3000
    // host: '0.0.0.0', // default: localhost
  },
  head: {
    link: [
      { rel: 'author', href: 'humans.txt' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicons/favicon.ico' },
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicons/favicon.ico',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/favicon-16.png',
        sizes: '16x16',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/favicon-32.png',
        sizes: '32x32',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-36.png',
        sizes: '36x36',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-48.png',
        sizes: '48x48',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-72.png',
        sizes: '72x72',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-144.png',
        sizes: '144x144',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-192.png',
        sizes: '192x192',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-256.png',
        sizes: '256x256',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicons/android-chrome-512.png',
        sizes: '512x512',
      },
      {
        rel: 'apple-touch-icon',
        href: '/favicons/apple-touch-icon.png',
      },
    ],
  },

  css: [
    '@aasaam/noto-font/dist/font-face.modern.css',
    '~/assets/styles/main.scss',
  ],

  plugins: [
    { src: '~/plugins/directives.js', mode: 'client' },
    { src: '~/plugins/leaflet.js', mode: 'client' },
  ],

  components: {
    dirs: [
      '~/components',
      '~/components/Structure',
      '~/components/Global',
      '~/components/FirstPage',
      '~/components/About',
      '~/components/OfferJobs',
      '~/components/Partners/Shahremohtava',
      '~/components/Products/Cms',
      '~/components/Products/Cms/Price',
      '~/components/Products/Htm',
      '~/components/Products/Football',
      '~/components/Products/Crypto',
      '~/components/Solutions/Dba',
      '~/components/Solutions/Monitoring',
      '~/components/Solutions/Vtp',
    ],
  },

  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
    'nuxt-purgecss',
    '@nuxtjs/google-analytics',
  ],

  googleAnalytics: {
    id: 'UA-60346188-1',
  },

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxt/content',
    ['@nuxtjs/i18n', I18N],
  ],

  axios: {},

  pwa: {
    manifest: false,
    icon: false,
    meta: false,
    workbox: {
      workboxURL:
        'https://cdn.jsdelivr.net/npm/workbox-cdn@5.1.4/workbox/workbox-sw.js',
      importScripts: [],
      config: { debug: false },
      cacheOptions: {
        cacheId: 'bare-prod',
        directoryIndex: '/',
        revision: '5xudlRWpuflF',
      },
      clientsClaim: true,
      skipWaiting: true,
      cleanupOutdatedCaches: true,
      offlineAnalytics: false,
      preCaching: [{ revision: '5xudlRWpuflF', url: '/?standalone=true' }],
      runtimeCaching: [
        {
          urlPattern: '/_nuxt/',
          handler: 'CacheFirst',
          method: 'GET',
          strategyPlugins: [],
        },
        {
          urlPattern: '/',
          handler: 'NetworkFirst',
          method: 'GET',
          strategyPlugins: [],
        },
      ],
      offlinePage: null,
      pagesURLPattern: '/',
      offlineStrategy: 'NetworkFirst',
    },
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    optionsPath: './plugins/vuetify.js',
    defaultAssets: false,
    treeShake: true,
  },

  purgeCSS: {
    enabled: true,
    paths: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      './node_modules/vuetify/dist/vuetify.js',
    ],
    styleExtensions: ['.css', '.scss'],
    // whitelist: ['body', 'html', 'nuxt-progress', ''],

    whitelist: ['v-application', 'v-application--wrap', 'layout', 'row', 'col'],
    whitelistPatterns: [
      /^v-((?!application).)*$/,
      /^theme--*/,
      /.*-transition/,
      /^justify-*/,
      /^p*-[0-9]/,
      /^m*-[0-9]/,
      /^text--*/,
      /--text$/,
      /^row-*/,
      /^col-*/,
      /leaflet/,
      /marker/,
    ],
    whitelistPatternsChildren: [/^v-((?!application).)*$/, /^theme--*/],

    extractors: [
      {
        extractor: (content) => content.match(/[A-z0-9-:\\/]+/g) || [],
        extensions: ['html', 'vue', 'js'],
      },
    ],
  },

  build: {
    extractCSS: true,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true,
          },
        },
      },
    },
    babel: {
      presets({ isClient }, preset) {
        if (isClient) {
          preset[1].targets = {
            browsers: [
              'Chrome >= 60',
              'Safari >= 10.1',
              'iOS >= 10.3',
              'Firefox >= 54',
              'Edge >= 15',
            ],
          };
        }
        return [preset];
      },
    },
  },
};
