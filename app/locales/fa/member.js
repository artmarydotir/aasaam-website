module.exports = {
  artmary: {
    name: 'مریم حقگو',
    role: 'توسعه دهنده فول استک',
    description: '',
  },
  mhf: {
    name: 'محمدحسین فتاحی زاده',
    role: 'مدیر ارشد فناوری',
    description: '',
  },
  neshom: {
    name: 'محسن توحیدی پور',
    role: 'توسعه دهنده فول استک',
    description: '',
  },
  moosavi: {
    name: 'آرش موسوی',
    role: 'مدیر عامل (CEO)',
    description: '',
  },
  etemadi: {
    name: 'آرش اعتمادی',
    role: 'توسعه دهنده فول استک',
    description: '',
  },
  ef: {
    name: 'الهه فاضل',
    role: ' مدیر ارشد اجرایی (COO)',
    description: '',
  },
  milad: {
    name: 'میلاد صفایی',
    role: 'مدیر ارشد مارکتینگ (CMO)',
    description: '',
  },
  // vahid: {
    //   name: 'وحید فاضل',
  //   role: 'کارشناس امور مالی',
  //   description: '',
  // },
  npm: {
    name: 'نرگس پورمقدسی',
    role: 'توسعه‌دهنده ارشد فرانت‌اند',
    description: '',
  },
  // nazanin: {
  //   name: 'نازنین زارع',
  //   role: 'کارشناس ارتباطات',
  //   description: '',
  // },
  ali: {
    name: 'علی طوافی',
    role: 'توسعه دهنده بک‌اند',
    description: '',
  },
  maani: {
    name: 'مانی بیگی',
    role: 'متخصص ارشد علوم داده',
    description: '',
  },
  reza: {
    name: 'رضا شبرنگ',
    role: 'متخصص علوم داده',
    description: '',
  },
  mohammadreza: {
    name: 'سید محمدرضا رضویان',
    role: 'کارشناس علوم داده',
    description: '',
  },
  khashayar: {
    name: 'سید خشایار موسوی',
    role: 'توسعه دهنده بک‌اند',
    description: '',
  },
  arash: {
    name: 'آرش عبدوس',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  ehsan: {
    name: 'احسان تشکری',
    role: 'مدیر پشتیبانی و توسعه دهنده فرانت',
    description: '',
  },
  sara: {
    name: 'سارا پیروزوند',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  fateme: {
    name: 'فاطمه تشکری',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  khorsandi: {
    name: 'محمد خورسندی',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  matin: {
    name: 'متین حسین زاده',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  aidin: {
    name: 'آیدین نعمتی',
    role: 'دواپس',
    description: '',
  },
  bahar: {
    name: 'بهار ایثاری',
    role: 'دواپس',
    description: '',
  },
  ghazal: {
    name: 'غزال فرهمند',
    role: 'کارشناس محتوا',
    description: '',
  },
  saeed: {
    name: 'سعید دانایی',
    role: 'کارشناس محتوا',
    description: '',
  },
  mahyar: {
    name: 'مهیار مورونی',
    role: 'کارشناس اداری',
    description: '',
  },
  // hosein: {
  //   name: 'حسین موسوی',
  //   role: 'کارشناس ارشد فروش',
  //   description: '',
  // },
  khaligh: {
    name: 'فاطمه خلیق',
    role: 'کارشناس اداری',
    description: '',
  },
  hamid: {
    name: 'حمید ادبی',
    role: 'کارپرداز',
    description: '',
  },
  darya: {
    name: 'دریا راد',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  sima: {
    name: 'سیما خرم نهاد',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  amirhossein: {
    name: 'امیرحسین رضایی',
    role: 'طراح محصول',
    description: '',
  },
  elham: {
    name: 'الهام تقی پور',
    role: 'کارشناس محتوا',
    description: '',
  },
  tashakori: {
    name: 'محمد تشکری',
    role: 'کارشناس ارشد دواپس',
    description: '',
  },
  vahid: {
    name: 'وحید فاضل',
    role: 'حسابدار',
    description: '',
  },
  mahbobeh: {
    name: 'محبوبه قاسمی',
    role: 'توسعه‌دهنده فرانت‌اند',
    description: '',
  },
  nina: {
    name: 'نینا سلیم خانی',
    role: ' کارشناس سئو',
    description: '',
  },
  mahsa: {
    name: 'مهسا بابالوئی',
    role: ' کارشناس سئو',
    description: '',
  },
  niloofar: {
    name: 'نیلوفر فتاح',
    role: ' کارشناس سئو',
    description: '',
  }
};
