export const memberConst = [
  {
    id: 'mhf',
    avatar: 'mhf.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/muhammad-hussein-fattahizadeh-30775839/',
      },
      {
        title: 'github',
        link: 'https://github.com/mhf-ir',
      },
      {
        title: 'stackoverflow',
        link: 'https://stackoverflow.com/users/306852/sweb',
      },
    ],
  },
  {
    id: 'moosavi',
    avatar: 'moosavi.svg',
    socialmedia: [],
  },
  {
    id: 'ef',
    avatar: 'ef.svg',
    socialmedia: [],
  },
  {
    id: 'etemadi',
    avatar: 'etemadi.svg',
    socialmedia: [],
  },
  // {
  //   id: 'vahid',
  //   avatar: 'vahid.svg',
  //   socialmedia: [],
  // },
  // {
  //   id: 'nazanin',
  //   avatar: 'nazanin.svg',
  //   socialmedia: [],
  // },
  // {
  //   id: 'hosein',
  //   avatar: 'hosein.svg',
  //   email: '',
  //   socialmedia: [
    //     {
      //       title: 'linkedin',
      //       link: 'https://www.linkedin.com/in/hosein6626/',
      //     },
      //   ],
      // },
  {
    id: 'milad',
    avatar: 'milad.svg',
    email: '',
    socialmedia: [],
  },
  {
    id: 'neshom',
    avatar: 'neshom.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/neshom/',
      },
      {
        title: 'gitlab',
        link: 'https://gitlab.com/neshom/',
      },
    ],
  },
  {
    id: 'ehsan',
    avatar: 'ehsan.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/ehsan-tashakkori-0443941b3/',
      },
    ],
  },
  {
    id: 'artmary',
    avatar: 'artmary.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/maryam-haghgou-5755a0159/',
      },
      {
        title: 'github',
        link: 'https://github.com/artmarydotir',
      },
      {
        title: 'stackoverflow',
        link: 'https://stackoverflow.com/users/4494385/art-mary',
      },
    ],
  },
  {
    id: 'vahid',
    avatar: 'vahid.svg',
    email: '',
    socialmedia: [],
  },
  {
    id: 'khaligh',
    avatar: 'khaligh.svg',
    email: '',
    socialmedia: [],
  },
  {
    id: 'mahyar',
    avatar: 'mahyar.svg',
    email: '',
  },
  {
    id: 'hamid',
    avatar: 'hamid.svg',
    email: '',
  },
  {
    id: 'npm',
    avatar: 'npm.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/narges-pourmoghadasi/',
      },
      {
        title: 'github',
        link: 'https://github.com/nargespms',
      },
    ],
  },
  {
    id: 'khashayar',
    avatar: 'khashayar.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/khashayar-mousavi-b8b713185/',
      },
    ],
  },
  {
    id: 'ali',
    avatar: 'ali.svg',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/ali-tavafi-34a530136/',
      },
      {
        title: 'github',
        link: 'https://github.com/Tavafi',
      },
      {
        title: 'stackoverflow',
        link: 'https://stackoverflow.com/users/7396147/ali-tavafi',
      },
    ],
  },
  {
    id: 'maani',
    avatar: 'maani.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/maani-beigy-69454438/',
      },
      {
        title: 'github',
        link: 'https://github.com/MaaniBeigy',
      },
    ],
  },
  {
    id: 'tashakori',
    avatar: 'tashakori.svg',
    socialmedia: [],
  },
  {
    id: 'reza',
    avatar: 'reza.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'http://linkedin.com/in/reza-shabrang-maryaan-35466b9b',
      },
      {
        title: 'github',
        link: 'https://github.com/rezashabrang',
      },
    ],
  },
  {
    id: 'mohammadreza',
    avatar: 'mohammadreza.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/mohammadreza-razavian-9748b1211/',
      },
      {
        title: 'github',
        link: 'https://github.com/smrrazavian',
      },
      {
        title: 'stackoverflow',
        link: 'https://stackoverflow.com/users/16433247/mohammadreza-razavian',
      },
    ],
  },
  {
    id: 'bahar',
    avatar: 'bahar.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/bahar-issari-a964071b3',
      },
    ],
  },
  {
    id: 'aidin',
    avatar: 'aidin.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'www.linkedin.com/in/aydin-nemati-5374421b3',
      },
    ],
  },
  {
    id: 'fateme',
    avatar: 'fateme.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/fatemeh-tashakkori-2250116b/',
      },
    ],
  },
  {
    id: 'arash',
    avatar: 'arash.svg',
    socialmedia: [],
  },
  {
    id: 'sara',
    avatar: 'sara.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/sara-piroozvand-1b70b5116',
      },
    ],
  },
  {
    id: 'khorsandi',
    avatar: 'khorsandi.svg',
    email: '',
  },
  {
    id: 'matin',
    avatar: 'matin.svg',
    email: '',
  },
  {
    id: 'mahbobeh',
    avatar: 'mahbobeh.svg',
    email: 'mah9372gh@gmail.com',
    socialmedia: [],
  },
  {
    id: 'darya',
    avatar: 'darya.svg',
    email: 'darya.raadd@gmail.com',
  },
  {
    id: 'sima',
    avatar: 'sima.svg',
    email: 'simakhoram7@gmail.com',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/simakhoram/',
      },
    ],
  },
  {
    id: 'ghazal',
    avatar: 'ghazal.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/ghazaal-farahmand-24581a1b2/',
      },
    ],
  },
  {
    id: 'saeed',
    avatar: 'saeed.svg',
    email: 'danaeesaeed@gmail.com',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/saeed-danaee-a133a921b/',
      },
    ],
  },
  {
    id: 'elham',
    avatar: 'elham.svg',
    email: '',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/elham-taghipour-803395216',
      },
    ],
  },
  {
    id: 'amirhossein',
    avatar: 'amirhossein.svg',
    email: 'im.amirezaei@gmail.com',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/amir-rezaei-410877112/',
      },
    ],
  },
  {
    id: 'nina',
    avatar: 'nina.svg',
    email: 'nilo0far7h6@gmail.com',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'http://www.linkedin.com/in/nina-salimi-82bb60174',
      },
    ],
  },
  {
    id: 'mahsa',
    avatar: 'mahsa.svg',
    email: '',
    socialmedia: [],
  },
  {
    id: 'niloofar',
    avatar: 'niloofar.svg',
    email: 'niloo.fattah91@gmail.com',
    socialmedia: [
      {
        title: 'linkedin',
        link: 'https://www.linkedin.com/in/niloufar-f-1220a3130',
      },
    ],
  },
];
