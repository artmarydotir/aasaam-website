module.exports = {
  artmary: {
    name: 'Maryam Haghgoo',
    role: 'Front-end Developer',
    description: 'aaa',
  },
  mhf: {
    name: 'Muhammad Hussein Fattahizadeh',
    role: 'Chief Technology Officer',
    description:
      '"Sharing knowledge is the most fundamental act of friendship. Because it is a way you can give something without loosing something."; Richard Stallman',
  },
  neshom: {
    name: 'Mohsen Tohidipour',
    role: 'Chief Communications Officer',
    description: '',
  },
  moosavi: {
    name: 'Arash Moosavi',
    role: 'Chief Executive Officer',
    description: '',
  },
  behzad: {
    name: 'Behzad Nooraee',
    role: 'Chief Commercial officer',
    description: '',
  },
  ef: {
    name: 'Elaheh Fazel',
    role: 'Chief Financial Officer',
    description: '',
  },
  npm: {
    name: 'Narges PourMoghadasi',
    role: 'Front-end Developer',
    description: '',
  },
  nazanin: {
    name: 'Nazanin Zare',
    role: 'Communications Expert',
    description: '',
  },
  // tashakori: {
  //   name: 'Mohammad Tashakori',
  //   role: 'System Administrator',
  //   description: '',
  // },
  maani: {
    name: 'Maani Beigy',
    role: 'Data scientists',
    description: '',
  },
  khashayar: {
    name: 'Khashayar Moosavi',
    role: 'Back-end Developer',
    description: '',
  },
  arash: {
    name: 'Arash Abdoos',
    role: 'Front-end Developer',
    description: '',
  },
  ehsan: {
    name: 'Ehsan Tashakori',
    role: 'Front-end Developer',
    description: '',
  },
  sara: {
    name: 'Sara Pirouzvand',
    role: 'Front-end Developer',
    description: '',
  },
  fateme: {
    name: 'Fateme Tashakori',
    role: 'Front-end Developer',
    description: '',
  },
  aidin: {
    name: 'Aidin Nemati',
    role: 'Devops',
    description: '',
  },
  bahar: {
    name: 'Bahar Issari',
    role: 'Devops',
    description: '',
  },
};
