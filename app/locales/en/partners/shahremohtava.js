module.exports = {
  description:
    'Shahre Mohtava is an inbound marketing agency that helps your business grow by providing free training, consulting, and inbound marketing services.',
  whatIsMarketing: {
    title: 'What is Inbound Marketing?',
    description:
      'Inbound marketing is a relatively new approach to marketing that focuses on "attracting customers" to sell services and products through content marketing. This marketing approach, sometimes called "hijacking marketing", improves customer experience and builds trust in the brand by producing valuable content that meets the content needs of the target community. Compared to traditional marketing, which is extroverted in nature and is known as Outbound Marketing, introverted marketing "reverses the relationship between buyer and seller." This means that in traditional marketing, the supplier or seller tries to find its customers by advertising and promoting their products through various channels, but in introverted marketing, awareness through content production leads to Customers find the supplier company.',
  },
  services: {
    title:
      'Shahre Mohtava, as an inbound marketing agency, provides the following services to Internet businesses and news and content sites:',
    items: [
      {
        title:
          'SEO consulting (from analyzing your current situation and that of your competitors to developing an SEO strategy)',
      },
      {
        title:
          'Content production (from content ideation and clustering or Topic Clustering to producing quality and unique content)',
      },
      {
        title:
          'Content production consulting (from analyzing competitors and audiences to developing a strategy and content calendar)',
      },
    ],
  },
  how: {
    title: 'Core values of Shahre Mohtava',
    description:
      'Above all, "content" is the most valuable concept in the Shahre Mohtava. "Learning" on the one hand and "innovation" on the other help us navigate the endless world of content. The "customer" as the main determinant of the direction of introverted marketing is the Shahre Mohtava guide. Adherence to "ethics" and guaranteeing the highest "quality" also make the brand image of the Shahre Mohtava.',
  },
};
