module.exports = {
  title:
    'aasaam Software Group needs full specialists to develop technical infrastructure',
  join: 'Join Our Team',
  jobMsg1: 'We Are Hiring',
  jobMsg2:
    'Our software group intends to attract programming experts with the following capabilities to complete its human resources in the development of its infrastructure.',
  jobMsg3:
    'Achieving valuable goals will only emerge by creating a great team. People who are looking for a job in a very intimate and calm environment and have the necessary conditions for one of the requested job positions and are confident in proving their skills can send their complete and updated resume to info@gowebsite.ir Please send. We are waiting for you in aasaam family, join us.',
  benefits: {
    title: 'Benefits',
    item: [
      {
        title: 'Free breakfast',
      },
      {
        title: 'Friendly working environment',
      },
      {
        title: 'Competitive salaries',
      },
      {
        title: 'On-Time Payment',
      },
      {
        title: 'Regular and supplementary Insurance',
      },
      {
        title: 'Accessible working place in Tehran',
      },
      {
        title: 'Work on the latest cutting edge technologies',
      },
      {
        title: 'Learning and Mentoring',
      },
    ],
  },
  call: 'Call Us',
  positions: [
    {
      title:
        '👨🏽‍💻👩🏻‍💻aasaam software development group (aasaam.com) is currently looking for creative and passionate candidates to fill the position of Front-end:',
      list: {
        all: {
          title: 'All candidates will have:',
          item: [
            'Hands-on Experience with Git',
            'Experience with Node.js environment (npm, yarn)',
            'Knowlege of REST and GraphQL',
            'Experience with Vue.js (Framework like Vuetify)',
            'Some experience with SSR and Nuxt.js',
          ],
        },
        ideally: {
          title: 'Ideally, candidates will also have experience with:',
          item: [
            'Linux, Bash',
            'CI and container environment',
            'Knowlege of REST and GraphQL',
            'Testing approach like TDD and BDD, Unit and E2E',
            'Scrum with Agile team',
          ],
        },
        personal: {
          title: 'Personal skills:',
          item: [
            'Fluent in English, reading and listening',
            'Proven track record of always learning',
            'Proactive attitude',
          ],
        },
      },
    },
    {
      title:
        '👨🏽‍💻👩🏻‍💻aasaam software development group (aasaam.com) is currently looking for creative and passionate candidates to fill the position of Back-end:',
      list: {
        all: {
          title: 'All candidates will have:',
          item: [
            'Hands-on Experience with Linux, Git, Docker and Bash',
            'Experience with CI and Container Environment',
            'Great Experience with Node.js as Backend developer',
            'Good knowledge about REST, GraphQL',
            'knowledge and Experience with MariaDB, Postgres, MongoDB, ElasticSearch, Redis, InfluxDB',
          ],
        },
        ideally: {
          title: 'Ideally, candidates will also have experience with:',
          item: [
            'Fastify, Molecular',
            'CI and container environment',
            'Testing approach like TDD, Unit and Integration',
            'Scrum with Agile team',
          ],
        },
        personal: {
          title: 'Personal skills:',
          item: [
            'Fluent in English, reading and listening',
            'Proven track record of always learning',
            'Proactive attitude',
          ],
        },
      },
    },
    {
      title:
        '👨🏽‍💻👩🏻‍💻aasaam software development group (aasaam.com) is currently looking for creative and passionate candidates to fill the position of DevOps:',
      list: {
        all: {
          title: 'All candidates will have:',
          item: [
            'Hands-on Experience with Linux, Git, Docker, Python and Bash',
            'Experience with automation like Ansible and SaltStack',
            'IaC in Mind',
            'Good knowledge about Network topology',
            'Good knowledge about Bare metal',
            'knowledge about and Experience with Windows Server, Linux Server(Mostly Debian Based)',
          ],
        },
        ideally: {
          title: 'Ideally, candidates will also have experience with:',
          item: [
            'Prometheus, ELK Stack, TICK Stack',
            'Metric collectors like Telegraf and Prometheus Exporters',
            'Log collectors like fluntbit and vector',
            'Grafana, Kibana and Modern analytic dashboard',
            'Consul, Nginx',
            'Scrum with Agile team',
          ],
        },
        personal: {
          title: 'Personal skills:',
          item: [
            'Fluent in English, reading and listening',
            'Proven track record of always learning',
            'Proactive attitude',
          ],
        },
      },
    },
  ],
};
