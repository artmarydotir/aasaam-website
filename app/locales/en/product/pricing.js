module.exports = {
  priceGoal: {
    title: 'factors affecting the cost of designing news sites',
    description:
      'many variables affect the cost of setting up and maintaining news sites. Appearance design, content management system programming, server hardware, monitoring and technical support are the most important things that we will briefly explain about each of them:',
    items: [
      {
        title: 'Dedicated design cost of news site appearance ',
        desc: 'Aasaam technical team designs the appearance of the news site specifically based on your desired graphic design and does not use any ready-made template for this task. Even if you like a ready-made template, we design and implement your own template similar to the one you like, using the latest web design technologies and to your liking.',
      },
      {
        title: 'Cost of content management system (CMS) programming news site',
        desc: 'Aasaam system uses a dedicated content management system that has been programmed by the technical team of the same collection and based on several years of experience in supporting news sites, various features have been added to it and are constantly updated.',
      },
      {
        title: 'Cost of news site server hardware',
        desc: 'Aasaam news system is provided as "software as a service" or SaaS. In this approach, you will not have to worry about hardware issues and server management in any way, and all the steps related to server maintenance and maintenance will be the responsibility of Aasaam. ',
      },
      {
        title: 'Cost of news system modules and facilities',
        desc: 'All modules and system features are the same for all Aasaam sites and all updates will be applied to all plans.',
      },
      {
        title: 'Cost of monitoring and technical support of the news site',
        desc: 'Aasaam is technical team monitors all servers and news sites in the collection 24 hours a day and fixes many of the problems that occur naturally in the site is operation, before you even notice the problems yourself Also, changes or problems that are reported by you, will be promptly reviewed by the Aasaam Support Unit and the necessary actions will be taken. ',
      },
      {
        title: 'The cost of visiting a news site',
        desc: 'Pricing of Aasaam news system plans based on the number of visits to your site. Because the less visits to your site, the lower the cost of monitoring and support for us. In other words, you will only pay more if you earn more through higher visits. ',
      },
    ],
  },
  priceTable: {
    title: 'News site design cost table',
    DedicatedDesignCost: 'Dedicated design cost',
    dailyHits: 'Daily hits',
    monthlyPrice: 'Monthly price',
    completeTransferCurrentContent: 'Complete transfer of current content',
    completeSystemTransfer: 'Complete system transfer',
    dedicatedDesign: 'Dedicated design',
    strongAndFastServer: 'Strong and fast server',
    technicalSupport: 'technical support',
    to250Hits: 'Up to 250,000 daily hits',
    to50Hits: 'Up to 50,000 daily hits',
    to100Hits: 'Up to 100,000 daily hits',
    to500Hits: 'Up to 500,000 daily hits',
    to1milionHits: 'Up to 1 million daily hits',
    to1500milionHits: 'Up to 1.5 million daily hits',
    to2milionHits: 'Up to 2 million daily hits',
    to3milionHits: 'Up to 3 million daily hits',
    to4milionHits: 'Up to 4 million daily hits',
    p45milionT: '45 million tomans',
    p75milionT: '75 million dollar',
    p7800T: '7,800,000 T',
    p9800T: '9,800,000 T',
    p14800T: '14,800,000 T',
    p19800T: '19,800,000 T',
    p24800T: '24,800,000 T',
    p29800T: '29,800,000 T',
    p34800T: '34,800,000 T',
    p140D: '140 Dollar',
    p200D: '200 Dollar',
    p280D: '280 Dollar',
    p360D: '360 Dollar',
    p520D: '520 Dollar',
    p690D: '690 Dollar',
    p860D: '860 Dollar',
    p1030D: '1030 Dollar',
    p1200D: '1200 Dollar',
    p50: '6,800,000 T',
    p100: '9,800,000 T',
    p250: '14,800,000 T',
    p500: '18,800,000 T',
    p1000: '25,800,000 T',
    p1500: '29,800,000 T',
    p2000: '36,800,000 T',
    p3000: '47,800,000 T',
    p4000: '57,800,000 T',
  },
  priceQuestionAnswer: {
    title: 'Answers to frequently asked questions',
    question1:
      'Is it possible to install aasaam news system on our corporate servers?',
    answer1:
      'Based on numerous experiences, our strong recommendation is to leave the provision and management of the server to aasaam technical team. But if your organization has special protocols for server provision, aasaam can set up and support a news system on your server if it has full access to the server.',
    question2: 'Will the monthly cost be reduced if the server is provided?',
    answer2:
      'Aasaam news system is provided in the form of a software service (SaaS) and a monthly fee is received for the technical support of the software. Maintaining and supporting servers sourced from outside aasaam will be more complicated. For this reason, in some cases, the cost of supporting servers outside aasaam may even exceed the announced costs.',
    question3: 'Are the system features the same in all price plans?',
    answer3:
      'Yes. Pricing is based on daily visits and all system modules are provided in all plans.',
    question4: 'How is the contract period and its renewal price determined?',
    answer4:
      'A one-year support contract will be concluded and it will be possible to extend the contract at the prices of the extension time.',
    question5: 'How long does it take to design and launch a news site?',
    answer5:
      'It takes 25 working days from the date the employer plan is approved to implement the plan and launch the news site.',
    question6: 'Will our current content be moved to a new news site?',
    answer6:
      'Yes. aasaam will transfer all your current content to a new site without the need for you to provide a database.',
    question7:
      'How much does it cost to transfer existing content to a new news site?',
    answer7:
      'The transfer of content to the new news site is done by aasaam for free.',
    question8:
      'How long does it take to transfer existing content to a new news site? ',
    answer8:
      'It is not possible to announce a specific interval for the content transfer period. The volume of content, the quality of its display on the site and the details desired by the employer, affect the transfer time.',
    question9: 'Where do I start to order a news site design?',
    answer9:
      'Our partners in the sales department are ready to answer you. Call 02191008212.',
    question10:
      'Will moving the site from another CMS to Aasaam reduce the number of visits and damage to the SEO?',
    answer10:
      'The transfer of the site, followed by the restructuring of the page address, should be done in compliance with the principles of SEO to reduce its risks. Aasaam technical team has a very good experience of transferring dozens of sites and carefully follows all SEO rules. Many sites, after being transferred to Aasaam, have gained a better position in SEO technical, and as a result, their keyword rankings and organic visits have improved.',
  },
};
