module.exports = {
  vtp: 'Enterprise Client Solutions (VTP)',
  whatIsVtp: {
    title: 'What is VTP?',
    description:
      'This solution, along with its related services, includes a hardware product with software services that allow companies and organizations to provide low-cost, low-maintenance desktops to their users. This solution is provided with integrated organizational services, central monitoring and central document and file exchange system',
  },
  featureVtp: {
    title: 'What are the uses of this solution?',
    description:
      'In many organizations, there is a security requirement to separate the network and office and organizational systems from individual and personal use. So that sensitive enterprise systems should be completely separate from systems that users are connected to the Internet. For this purpose, you can use Web Surfing, work with multimedia files, use office software (Word, Excel, 2), share and move files, etc. in an environment completely separate from computers connected to sensitive enterprise systems. Also, by using familiar security mechanisms in GNU / Linux-based operating systems, the security of enterprise users on the Internet can be greatly enhanced.',
  },
  witchHardware: {
    title: 'What is the hardware provided in this solution?',
    description:
      'A small computer is slightly larger than a bank card that allows you to connect two monitors at the same time, a keyboard, mouse and audio jack to connect speakers and headphones. Using a network card or wireless, it is possible to connect to the Internet, which by means of dominant browsers such as Firefox and Chrome, users can easily use the Internet like desktops. The platform also includes office software such as word processing for working on WORD files, spreadsheets, presentation files, etc. One of the most important advantages of this hardware is its small size (about 85.6mm × 56.5mm), high power consumption He mentioned low (5 volts, 3 amps and under normal circumstances 3.4 watts) and much lower cost compared to desktop computers.',
    hardWareConfig: {
      title: 'The hardware specifications of these small computers are as follows',
      items: [
        {
          title: 'Processor: Quad core Cortex-A72 (ARM v8) 1.5GHz',
        },
        {
          title: 'Memory: 2 - 4 GB DDR4',
        },
        {
          title: 'Network card: Gigabit Ethernet',
        },
        {
          title: 'Wireless: 2.4 GHz and 5.0 GHz IEEE 802.11ac',
        },
        {
          title: 'Image: 2 × micro-HDMI ports (up to 4k supported)',
        },
        {
          title: 'Available USB ports: 2 * USB 3.0 + 2 * USB 2.0',
        },
        {
          title: 'Power: DC 5V 3A',
        },
        {
          title: 'Cooling system: Smart Cooling Fan',
        },
      ],
    },
  },
  services: {
    title: 'What are the services provided in this solution?',
    description:
      'Among the services provided in this way, the following can be mentioned:',
    items: [
      {
        title: 'Monitoring:',
        desc:
          'Integrated and centralized monitoring of all these computers at the hardware and software level to monitor their performance',
      },
      {
        title: 'Software Software Central Service:',
        desc:
          'With this service, changes can be made centrally, including changes in files (adding, deleting and editing files centrally), installing and updating software, and many other remote controls on all or part of the computer.',
      },
      {
        title: 'Centralized Update Service:',
        desc:
          'Through which the operating system and all software can be updated centrally on all or part of the computers by saving bandwidth.',
      },
      {
        title: 'Central document and file exchange system:',
        desc:
          'Using this service, you can provide central systems for sharing document files, multimedia and other types of files. Those who have used familiar systems such as Google Docs or Dropbox will find the system to work very similarly to them.',
      },
      {
        title: 'Organizational user management:',
        desc:
          'In this way, to manage the authentication of users, the use of Active Directory or OpenLDAP services is considered.',
      },
    ],
  },
  os: {
    title: 'What is the operating system of these computers?',
    description:
      'The operating system installed on these computers is a customized enterprise operating system based on GNU / Linux. With a very powerful graphical interface, it almost completely allows individuals to use it organizationally and personally.',
  },
  cardHardware: {
    title: 'What hardware is used for this solution?',
    items: [
      'For this solution, the most famous single board computer in the IT industry called Raspberry Pi has been used.',
      'The fan for the cooling system is installed in such a way that it starts working only when the temperature reaches a certain degree and keeps the hardware at the optimum operating point temperature.',
      'Its case is custom designed for ease of use',
      'A 5-volt, 3-amp adapter is provided for power supply',
    ],
  },
  tech: {
    title: 'What technologies have we used in this solution?',
    items: [
      {
        title: 'Monitoring:',
        desc: 'TIG Stack (Telegraf, Grafana, InfluxDB)',
      },
      {
        title: 'Software Software Central Service:',
        desc: 'Saltstack',
      },
      {
        title: 'Central document and file exchange system:',
        desc: 'NextCloud',
      },
      {
        title: 'User management:',
        desc: 'Active Directory, OpenLDAP',
      },
      {
        title: 'Computer operating system:',
        desc: 'Debian GNU/Linux',
      },
      {
        title: 'Office software:',
        desc: 'LibreOffice',
      },
      {
        title: 'Multimedia:',
        desc: 'VLC',
      },
      {
        title: 'Browser:',
        desc: 'Firefox, Chrome',
      },
    ],
  },
};
