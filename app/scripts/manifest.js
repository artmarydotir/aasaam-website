const fs = require('fs');
const path = require('path');

const { Organization } = require('@aasaam/information');

Object.keys(Organization).forEach((lang) => {
  const d = Organization[lang];
  const p = path.resolve(__dirname, `../static/manifest-${lang}.json`);
  const manifest = {
    name: `${d.name}`,
    short_name: `${d.legalName}`,
    theme_color: '#f37921',
    scope: '/',
    lang,
    start_url: '/',
    orientation: 'portrait',
    display: 'standalone',
    background_color: '#f37921',
    description: `${d.description}`,
    categories: d.mainEntityOfPage.keywords.split(','),
    icons: [
      {
        type: 'image/png',
        src: '/favicons/favicon-16.png',
        sizes: '16x16',
        purpose: 'any maskable',
      },
      {
        type: 'image/png',
        src: '/favicons/favicon-32.png',
        sizes: '32x32',
        purpose: 'any maskable',
      },
      {
        type: 'image/png',
        src: '/favicons/android-chrome-36.png',
        sizes: '36x36',
        purpose: 'any maskable',
      },
      {
        type: 'image/png',
        src: '/favicons/android-chrome-48.png',
        sizes: '48x48',
        purpose: 'any maskable',
      },
      {
        type: 'image/png',
        src: '/favicons/android-chrome-72.png',
        sizes: '72x72',
        purpose: 'any maskable',
      },
      {
        type: 'image/png',
        src: '/favicons/android-chrome-144.png',
        sizes: '144x144',
        purpose: 'any maskable',
      },
      {
        type: 'image/png',
        src: '/favicons/android-chrome-192.png',
        sizes: '192x192',
        purpose: 'any maskable',
      },
      {
        type: 'image/png',
        src: '/favicons/android-chrome-256.png',
        sizes: '256x256',
        purpose: 'any maskable',
      },
      {
        type: 'image/png',
        src: '/favicons/android-chrome-512.png',
        sizes: '512x512',
        purpose: 'any maskable',
      },
      {
        rel: 'apple-touch-icon',
        src: '/favicons/apple-touch-icon.png',
        purpose: 'any maskable',
      },
      {
        src: '/favicons/android-chrome-192.png',
        sizes: '192x192',
        type: 'image/png',
        purpose: 'any maskable',
      },
    ],
  };
  fs.writeFileSync(p, `${JSON.stringify(manifest, null, 2)}`);
});
